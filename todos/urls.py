from django.urls import path

from todos.views import (
  TodoListView,
  TodoListDetailView,
  TodoListCreateView,
  TodoItemCreateView,
  TodoListDeleteView,
  TodoListUpdateView,
  TodoItemUpdateView,
  )


urlpatterns = [
    path("", TodoListView.as_view(), name="todo_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_detail"),
    path("new/", TodoListCreateView.as_view(), name="todo_new"),
    path("items/new/", TodoItemCreateView.as_view(), name="todo_item_new"),
    path("<int:pk>/items/edit/", TodoItemUpdateView.as_view(), name="todo_item_edit"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todo_edit"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_delete"),
]