from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from todos.models import TodoList, TodoItem

# Create your views here.
class TodoListView(ListView):
  model = TodoList
  template_name = "todos/list.html"

  def get(self, request, *args, **kwargs):
    response = super(ListView, self).get(request, *args, **kwargs)
    response.status_code = 200
    return response


class TodoListDetailView(DetailView):
  model = TodoList
  template_name = "todos/detail.html"

  def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class TodoListCreateView(CreateView):
  model = TodoList
  template_name = "todos/new.html"
  fields = ['name']
  success_url = reverse_lazy("todo_list")


class TodoListDeleteView(DeleteView):
  model = TodoList
  template_name = "todos/delete.html"
  success_url = reverse_lazy("todo_list")


class TodoListUpdateView(UpdateView):
  model = TodoList
  template_name = "todos/edit.html"
  fields = ['name']
  success_url = reverse_lazy("todo_list")


class TodoItemCreateView(CreateView):
  model = TodoItem
  template_name = "items/new.html"
  fields = ['task', 'due_date', 'is_completed', 'list']
  success_url = reverse_lazy("todo_list")


class TodoItemUpdateView(UpdateView):
  model = TodoItem
  template_name = "items/edit.html"
  fields = ['task', 'due_date', 'is_completed', 'list']
  success_url = reverse_lazy("todo_list")

  # TodoListCreateView,
  # TodoItemCreateView,
  # TodoDeleteView,
  # TodoUpdateView,