from django.db import models
from django.utils import timezone
# Create your models here.

class TodoList(models.Model):
  name = models.CharField(unique=True, max_length=100)
  created_on = models.DateTimeField(auto_now_add=True)

  def __str__(self):
      return self.name


class TodoItem(models.Model):
  task = models.CharField(max_length=100)
  due_date = models.DateTimeField(blank=True, default=timezone.now, null=True)
  is_completed = models.BooleanField(null=False, default=False)
  list = models.ForeignKey(
    'ToDoList',
    related_name="items",
    on_delete=models.CASCADE,
  )

  def __str__(self):
      return self.task

